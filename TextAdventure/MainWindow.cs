﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextAdventure.BaseSystem.Base;

namespace TextAdventure
{
    public partial class MainWindow : Form
    {
        public static MainWindow Instance { get; private set; }
        public Area CurrentArea
        {
            get
            {
                return AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID);
            }
        }

        private bool IsInPlayerInventory = false;

        public Game.Actors.Player Player = new Game.Actors.Player();

        public BaseSystem.Base.AreaHandler AreaHandler = new BaseSystem.Base.AreaHandler();
        public BaseSystem.Base.ActorHandler ActorHandler = new BaseSystem.Base.ActorHandler();

        private MainData CurrentProgress = new MainData();

        private BaseSystem.Base.Item useItemWithItem = null;

        private bool didLoad = false;

        public MainWindow()
        {
            Instance = this;
            InitializeComponent();

            LoadProgress();

            AreaHandler.InitializeAreas();
            AreaHandler.LoadAreaData();

            ActorHandler.InitializeActors();
            ActorHandler.LoadActorData();
            
            Player.LoadData();

            ResetDescriptionBox();

            ItemList.DataSource = Player.Inventory;

            IsInPlayerInventory = true;

            for (int i = 0; i < CurrentProgress.DisplayedOutput.Count; i++)
            {
                WriteLine(CurrentProgress.DisplayedOutput[i], false);
            }

            if (didLoad)
            {
                WriteLine("===========");
                WriteLine("GAME LOADED");
            }

            BaseSystem.Base.Area area = AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID);
            LoadArea(area);
            
            TextBox.SelectionStart = TextBox.TextLength;
            TextBox.ScrollToCaret();
            
            toolTip.SetToolTip(Look, "Examine your surroundings");
        }

        private void ResetUseWith()
        {
            useItemWithItem = null;
            UseWithBtn.Text = "Use with...";
        }

        public void LoadArea(BaseSystem.Base.Area area)
        {
            CurrentProgress.CurrentAreaID = area.AreaID;
            
            ItemDescription.Text = "";

            RefreshAll();

            RefreshAll();

            RefreshDirections();

            WriteLine("");
            WriteLine("---------------");
            WriteLine(area.GetLookDescription());
            WriteLine("");
        }

        private void RefreshDirections()
        {
            Directions.DataSource = CurrentArea.Connections.FindAll(o => o.Availability != Area.Direction.DirectionStates.Hidden);
        }

        private void LoadProgress()
        {
            if(DataSaver.FileExists("Progress"))
            {
                didLoad = true;
                CurrentProgress = DataSaver.LoadFile<MainData>("Progress");
            }
        }

        private void SaveProgress()
        {
            CurrentProgress.SaveAsFile("Progress");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveProgress();
            AreaHandler.SaveAreaData();
            ActorHandler.SaveActorData();
            Player.SaveData();
        }

        public void WriteLine(string line, bool save = true)
        {
            TextBox.AppendText(line + Environment.NewLine);

            if (save)
            {
                CurrentProgress.DisplayedOutput.Add(line);
            }

            TextBox.SelectionStart = TextBox.TextLength;
            TextBox.ScrollToCaret();
        }

        public void WriteLineExamine(string line)
        {
            WriteLine("> " + line);
        }

        private void OpenInventory()
        {
            ResetDescriptionBox();
            ItemList.DataSource = Player.Inventory;

            IsInPlayerInventory = true;
            
            PickUpDropBtn.Text = "Drop";

            GiveItem.Enabled = true;
        }

        private void OpenWorldItems()
        {
            ResetDescriptionBox();
            ItemList.DataSource = AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID).Items;

            IsInPlayerInventory = false;

            PickUpDropBtn.Text = "Take";

            GiveItem.Enabled = false;
        }

        private void ItemList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (item == null)
                return;

            ItemDescription.Text = item.GetShortDescription();

            if (useItemWithItem == null)
            {
                PickUpDropBtn.Enabled = item.GetCanPickUp();
                ActivateBtn.Enabled = item.GetCanActivate();
                ExamineItemBtn.Enabled = true;
                UseWithBtn.Enabled = true;

                bool canPickUpDrop = (item.GetCanPickUp() && !IsInPlayerInventory) || (item.GetCanDrop() && IsInPlayerInventory);

                toolTip.SetToolTip(ActivateBtn, item.GetCanActivate() ? "Activate the " + item.GetName() : item.GetName() + " cannot be activated.");
                toolTip.SetToolTip(PickUpDropBtn, canPickUpDrop ? PickUpDropBtn.Text + " the " + item.GetName() : "Cannot " + PickUpDropBtn.Text.ToLower() + " the " + item.GetName());
                toolTip.SetToolTip(ExamineItemBtn, "Examine the " + item.GetName());

                toolTip.SetToolTip(UseWithBtn, "Use the " + item.GetName() + " on something.");
            }
            else
            {
                if (item != useItemWithItem)
                {
                    toolTip.SetToolTip(UseWithBtn, "Use the " + useItemWithItem.GetName() + " on the " + item.GetName());
                    UseWithBtn.Text = "Select";
                }
                else
                {
                    toolTip.SetToolTip(UseWithBtn, "Cancel");
                    UseWithBtn.Text = "Cancel";
                }

                toolTip.SetToolTip(ActivateBtn, "Cannot do this right now.");
                toolTip.SetToolTip(PickUpDropBtn, "Cannot do this right now.");
                toolTip.SetToolTip(ExamineItemBtn, "Cannot do this right now.");
            }

            SetUseItemOnActorTooltip();
            SetGiveItemToActorTooltip();
        }

        private void SetUseItemOnActorTooltip()
        {
            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (item == null)
            {
                toolTip.SetToolTip(InteractPersonBtn, "Select an item first!");
                return;
            }

            if (actor == null)
            {
                toolTip.SetToolTip(InteractPersonBtn, "Select someone to use the " + item.GetName() + " on first!");
                return;
            }
            
            toolTip.SetToolTip(InteractPersonBtn, "Use the " + item.GetName() + " on " + actor.GetName());
        }

        private void SetGiveItemToActorTooltip()
        {
            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (item == null)
            {
                toolTip.SetToolTip(GiveItem, "Select an item first!");
                return;
            }

            if (actor == null)
            {
                toolTip.SetToolTip(GiveItem, "Select someone to give the " + item.GetName() + " to first!");
                return;
            }

            toolTip.SetToolTip(GiveItem, "Give the " + item.GetName() + " to " + actor.GetName());
        }

        private void ResetDescriptionBox()
        {
            ItemDescription.Text = "";
            PersonDescription.Text = "";

            PickUpDropBtn.Enabled = false;
            ActivateBtn.Enabled = false;
            ExamineItemBtn.Enabled = false;

            if (useItemWithItem == null)
            {
                UseWithBtn.Enabled = false;
            }
        }

        private void ExamineItemBtn_Click(object sender, EventArgs e)
        {
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            item.DoExamineDescription();
        }

        private void PickUpDropBtn_Click(object sender, EventArgs e)
        {
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (IsInPlayerInventory)
            {
                Player.Inventory.Remove(item);
                AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID).Items.Add(item);

                WriteLine("You drop the " + item.GetName());

                ItemList.DataSource = null;
                ItemList.DataSource = Player.Inventory;

                ResetDescriptionBox();
            }
            else
            {
                if(item.GetCanPickUp())
                {
                    Player.Inventory.Add(item);
                    AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID).Items.Remove(item);

                    ResetDescriptionBox();

                    ItemList.DataSource = null;
                    ItemList.DataSource = AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID).Items;
                    
                    WriteLine("You take the " + item.GetName());
                }
                else
                {
                    WriteLine("You can't take this.");
                }
            }
        }

        public void RefreshAll()
        {
            RefreshActorList();
            RefreshItemList();
            RefreshDirections();
        }

        public void RefreshActorList()
        {
            People.DataSource = null;
            People.DataSource = ActorHandler.GetActorsInArea(AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID));
        }

        private void RefreshItemList()
        {
            ItemList.DataSource = null;

            if(IsInPlayerInventory)
            {
                ItemList.DataSource = Player.Inventory;
            }
            else
            {
                ItemList.DataSource = CurrentArea.Items;
            }
        }

        private void TalkToBtn_Click(object sender, EventArgs e)
        {
            ResetUseWith();

            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;

            if(actor != null)
            {
                actor.DoDialogue();
            }
        }

        private void ExaminePersonBtn_Click(object sender, EventArgs e)
        {
            ResetUseWith();

            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;

            if (actor != null)
            {
                actor.DoExamineDescription();
            }
        }

        private void GiveItem_Click(object sender, EventArgs e)
        {
            ResetUseWith();

            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (actor != null && item != null)
            {
                if(actor.TryReceiveItem(item))
                {
                    Player.Inventory.Remove(item);

                    ItemList.DataSource = null;
                    ItemList.DataSource = Player.Inventory;
                    ResetDescriptionBox();
                }
            }

            if (actor == null)
            {
                WriteLine("You need to select someone to give the item to first.");
            }

            if (item == null)
            {
                WriteLine("You need to select something to give first.");
            }
        }

        private void InteractPersonBtn_Click(object sender, EventArgs e)
        {
            ResetUseWith();

            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            if (actor != null && item != null)
            {
                actor.UseItemOnActor(item);
            }

            if (actor == null)
            {
                WriteLine("You need to select someone to use the item on first.");
            }

            if (item == null)
            {
                WriteLine("You need to select something to use first.");
            }
        }

        private void UseWithBtn_Click(object sender, EventArgs e)
        {
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            ResetDescriptionBox();

            if (useItemWithItem == null)
            {
                UseWithBtn.Text = "Select";
                UseWithBtn.Enabled = true;

                useItemWithItem = item;
            }
            else if(useItemWithItem == item)
            {
                ResetUseWith();
            }
            else
            {
                useItemWithItem.UseOnItem(item);
                ResetUseWith();
            }
            
            ItemList_SelectedIndexChanged(sender, e);
        }

        private void ActivateBtn_Click(object sender, EventArgs e)
        {
            BaseSystem.Base.Item item = (BaseSystem.Base.Item)ItemList.SelectedItem;

            item.DoActivateItem();
        }

        private void GoToPlace_Click(object sender, EventArgs e)
        {
            BaseSystem.Base.Area.Direction direction = (BaseSystem.Base.Area.Direction)Directions.SelectedItem;

            ResetUseWith();

            ResetDescriptionBox();

            RefreshAll();
            
            if (direction != null)
            {
                if (direction.Availability == Area.Direction.DirectionStates.Available)
                {
                    LoadArea(MainWindow.Instance.AreaHandler.GetAreaByID(direction.ConnectedAreaID));
                }
                else
                {
                    WriteLine(direction.LockedMessage);
                }
            }
        }

        private void ExamineDirection_Click(object sender, EventArgs e)
        {
            ResetUseWith();

            BaseSystem.Base.Area.Direction direction = (BaseSystem.Base.Area.Direction)Directions.SelectedItem;

            if (direction != null)
            {
                WriteLineExamine(direction.Description);
            }
        }

        private void People_SelectedIndexChanged(object sender, EventArgs e)
        {
            BaseSystem.Base.Actor actor = (BaseSystem.Base.Actor)People.SelectedItem;

            PersonDescription.Text = "";

            if (actor != null)
            {
                PersonDescription.Text = actor.GetShortDescription();

                toolTip.SetToolTip(TalkToBtn, "Talk to " + actor.GetName());
                toolTip.SetToolTip(ExaminePersonBtn, "Examine " + actor.GetName());
            }
            
            SetUseItemOnActorTooltip();
            SetGiveItemToActorTooltip();
        }

        private void Look_Click(object sender, EventArgs e)
        {
            WriteLine(AreaHandler.GetAreaByID(CurrentProgress.CurrentAreaID).GetLookDescription());
        }

        private void Directions_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolTip.SetToolTip(ExamineDirection, "Examine the selected direction");
            toolTip.SetToolTip(GoToPlace, "Go to the selected direction");
        }
        
        public class MainData
        {
            public List<string> DisplayedOutput = new List<string>();
            public int CurrentAreaID = 0;
        }

        private void itemTabs_Selected(object sender, TabControlEventArgs e)
        {
            if (itemTabs.SelectedIndex == 0)
                OpenInventory();

            if (itemTabs.SelectedIndex == 1)
                OpenWorldItems();
        }
    }
}
