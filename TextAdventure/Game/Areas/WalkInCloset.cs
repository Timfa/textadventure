﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class WalkInCloset : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A very small area. There are several shelves and empty coat hangers. The shelves are worn.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("WalkInCloset"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("WalkInCloset");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("WalkInCloset");
        }

        public override void SetupConnections()
        {
            ConnectTo("Master Bedroom", MainWindow.Instance.AreaHandler.FindAreaOfType<LargeBedroom>(), "Go out of the closet.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
