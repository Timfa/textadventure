﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class Kitchen : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A kitchen. Cleaner than expected, considering the wear and tear you see on the appliances. Smells dusty.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Kitchen"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("Kitchen");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("Kitchen");
        }

        public override void SetupConnections()
        {
            ConnectTo("Living Room", MainWindow.Instance.AreaHandler.FindAreaOfType<LivingRoom>(), "Back to the living room.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
