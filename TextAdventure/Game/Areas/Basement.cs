﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Areas
{
    public class Basement : BaseSystem.Base.Area
    {
        public Basement()
        {
            Items.Add(new BrokenFuseBox());
        }

        public override string GetLookDescription()
        {
            return "The basement is cold and dark. There is a small amount of light coming from the staircase.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Basement"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("Basement");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("Basement");
        }

        public override void SetupConnections()
        {
            ConnectTo("Staircase up", MainWindow.Instance.AreaHandler.FindAreaOfType<LivingRoom>(), "A staircase going up.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
