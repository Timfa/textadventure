﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.Game.Objects;

namespace TextAdventure.Game.Areas
{
    public class TestArea : BaseSystem.Base.Area
    { 
        public TestArea()
        {
        }

        public override string GetLookDescription()
        {
            return "An empty void used for testing. There's a house here.";
        }

        public override void Load()
        {
            if(DataSaver.FileExists("TestArea"))
            {
                TestAreaSave save = DataSaver.LoadFile<TestAreaSave>("TestArea");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            TestAreaSave save = new TestAreaSave();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("TestArea");
        }

        public override void SetupConnections()
        {
            ConnectTo("House", MainWindow.Instance.AreaHandler.FindAreaOfType<Hallway>(), "A house. The door seems to be unlocked.");

            ConnectTo("Into the void", MainWindow.Instance.AreaHandler.FindAreaOfType<TheOtherLand>(), "There doesn't seem to be much there, but you could go look anyway.");
        }

        public class TestAreaSave
        {
            public AreaItemData ItemData;
        }
    }
}
