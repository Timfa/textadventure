﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class LargeBedroom : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A fairly large bedroom. There is a large bed, a desk, and a small drawer.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("LargeBedroom"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("LargeBedroom");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("LargeBedroom");
        }

        public override void SetupConnections()
        {
            ConnectTo("Hallway", MainWindow.Instance.AreaHandler.FindAreaOfType<UpperHallway>(), "Back to the hallway");
            ConnectTo("Walk-in closet", MainWindow.Instance.AreaHandler.FindAreaOfType<WalkInCloset>(), "A closet large enough to enter.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
