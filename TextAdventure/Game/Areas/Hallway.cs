﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.Game.Areas
{
    public class Hallway : BaseSystem.Base.Area
    {
        public override string GetLookDescription()
        {
            return "A fairly small hallway. There's not much here. The coat-hangers are empty, but seem to have been used often.";
        }

        public override void Load()
        {
            if (DataSaver.FileExists("Hallway"))
            {
                AreaData save = DataSaver.LoadFile<AreaData>("Hallway");

                ApplyItemSaveData(save.ItemData);
            }
        }

        public override void Save()
        {
            AreaData save = new AreaData();

            save.ItemData = GetItemSaveData();

            save.SaveAsFile("Hallway");
        }

        public override void SetupConnections()
        {
            ConnectTo("Outside", MainWindow.Instance.AreaHandler.FindAreaOfType<TestArea>(), "The door back outside.");
            ConnectTo("Living Room", MainWindow.Instance.AreaHandler.FindAreaOfType<LivingRoom>(), "A door further into the house.");
        }

        public class AreaData
        {
            public AreaItemData ItemData;
        }
    }
}
