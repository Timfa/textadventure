﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class Toilet : BaseSystem.Base.Item
    {
        public override void DoActivateItem()
        {
            MainWindow.Instance.WriteLineExamine("The toilet flushes.");
        }

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("A white porcelain toilet. It's been cleaned recently, and smells surprisingly nice.");
        }

        public override bool GetCanActivate()
        {
            return true;
        }

        public override bool GetCanDrop()
        {
            return false;
        }

        public override bool GetCanPickUp()
        {
            return false;
        }
        
        public override string GetName()
        {
            return "Toilet";
        }

        public override string GetShortDescription()
        {
            return "It's a toilet.";
        }

        public override void UseOnItem(Item other)
        {
            if(other is Screwdriver)
            {
                MainWindow.Instance.WriteLine("The screwdriver is too heavy to flush. It's wet now.");
            }

            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
