﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Game.Areas;
using TextAdventure.Utility;

namespace TextAdventure.Game.Objects
{
    public class Faucet : BaseSystem.Base.Item
    {
        public override void DoActivateItem()
        {
            MainWindow.Instance.WriteLineExamine("Nothing seems to happen. Maybe if it were still attached to the bathtub, but you broke it off.");
        }

        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("A simple faucet.");
        }

        public override bool GetCanActivate()
        {
            return true;
        }

        public override bool GetCanDrop()
        {
            return true;
        }

        public override bool GetCanPickUp()
        {
            return true;
        }
        
        public override string GetName()
        {
            return "Faucet";
        }

        public override string GetShortDescription()
        {
            return "The faucet you broke off the bathtub.";
        }

        public override void UseOnItem(Item other)
        {
            if(other is Bathtub)
            {
                MainWindow.Instance.WriteLine("It's broken off, you can't put it back.");
            }

            if(other is BrokenFuseBox)
            {
                DestroyItem();
                other.DestroyItem();

                CurrentArea.Items.Add(new FuseBox());

                MainWindow.Instance.WriteLine("You bravely shove the metal Faucet into the broken electrical components of the Fuse Box. It makes a scary sound, but it's probably fixed, for now.");

                MainWindow.Instance.RefreshAll();

                MainWindow.Instance.AreaHandler.FindAreaOfType<Attic>().OnTurnOnLights();

                return;
            }

            MainWindow.Instance.WriteLine("Nothing really seems to happen.");
        }
    }
}
