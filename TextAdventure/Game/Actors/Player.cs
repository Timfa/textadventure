﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;

namespace TextAdventure.Game.Actors
{
    public class Player : BaseSystem.Base.Actor
    {
        public override void DoExamineDescription()
        {
            MainWindow.Instance.WriteLineExamine("It's you!");
        }

        public override void DoDialogue()
        {
            MainWindow.Instance.WriteLine("You mutter to yourself");
        }

        public override string GetNameKnown()
        {
            return "You!";
        }

        public override string GetNameUnknown()
        {
            return "You!";
        }

        public override string GetShortDescription()
        {
            throw new NotImplementedException();
        }

        public override void LoadData()
        {
            if (DataSaver.FileExists("Player"))
            {
                PlayerSaveData save = DataSaver.LoadFile<PlayerSaveData>("Player");

                ApplyActorData(save.InventoryData);
            }
        }

        public override void SaveData()
        {
            PlayerSaveData save = new PlayerSaveData();

            save.InventoryData = GetActorData();

            save.SaveAsFile("Player");
        }

        public override bool TryReceiveItem(Item item)
        {
            throw new NotImplementedException();
        }

        public override void UseItemOnActor(Item item)
        {
            throw new NotImplementedException();
        }

        public class PlayerSaveData
        {
            public ActorData InventoryData;
        }
    }
}
