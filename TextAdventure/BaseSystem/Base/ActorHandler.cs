﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAdventure.BaseSystem.Base;
using TextAdventure.Game.Actors;
using TextAdventure.Utility;

namespace TextAdventure.BaseSystem.Base
{
    public class ActorHandler
    {
        private List<Actor> actors = new List<Actor>();

        public void InitializeActors()
        {
            //Create the actors

            CreateActor(new TestMan());
        }

        public Actor FindItemOwner(Item item)
        {
            return actors.Find(o => o.Inventory.Contains(item));
        }

        public T FindActorOfType<T>() where T : Actor
        {
            Actor actor = actors.Find(o => o is T);

            if (actor == null)
            {
                return default(T);
            }

            return (T)actor;
        }

        public List<Actor> GetActorsInArea(BaseSystem.Base.Area area)
        {
            return actors.FindAll(o => o.LocationID == area.AreaID);
        }

        private Actor CreateActor(Actor baseActor)
        {
            actors.Add(baseActor);

            return baseActor;
        }

        public void LoadActorData()
        {
            for (int i = 0; i < actors.Count; i++)
            {
                actors[i].LoadData();
            }
        }

        public void SaveActorData()
        {
            for (int i = 0; i < actors.Count; i++)
            {
                actors[i].SaveData();
            }
        }
    }
}
