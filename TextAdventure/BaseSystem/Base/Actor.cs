﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextAdventure.BaseSystem.Base
{
    public abstract class Actor
    {
        public List<Item> Inventory = new List<Item>();

        public abstract void SaveData();
        public abstract void LoadData();

        public string GetName()
        {
            return Known ? GetNameKnown() : GetNameUnknown();
        }

        public abstract string GetNameKnown();
        public abstract string GetNameUnknown();

        public bool Known
        {
            get
            {
                return known;
            }

            set
            {
                known = value;

                MainWindow.Instance.RefreshAll();
            }
        }

        private bool known = false;

        public abstract void DoDialogue();

        public abstract string GetShortDescription();
        public abstract void DoExamineDescription();

        public abstract bool TryReceiveItem(Base.Item item);

        public int LocationID = 0;
        
        public BaseSystem.Base.Area CurrentArea
        {
            get
            {
                return MainWindow.Instance.AreaHandler.GetAreaByID(LocationID);
            }

            set
            {
                LocationID = value.AreaID;
            }
        }

        public void Speak(string dialogue)
        {
            MainWindow.Instance.WriteLine(GetName() + ": \"" + dialogue + "\".");
        }

        public override string ToString()
        {
            return GetName();
        }

        protected ActorData GetActorData()
        {
            ActorData data = new ActorData();

            data.Known = Known;

            data.Items = new ItemSaveEntry[Inventory.Count];

            data.LocationID = LocationID;

            for (int i = 0; i < Inventory.Count; i++)
            {
                data.Items[i] = new ItemSaveEntry();
                data.Items[i].ItemType = Inventory[i].GetItemID();
                data.Items[i].SaveData = Inventory[i].SaveData;
            }

            return data;
        }

        protected void ApplyActorData(ActorData data)
        {
            Inventory = new List<Item>();

            Known = data.Known;
            LocationID = data.LocationID;

            for (int i = 0; i < data.Items.Length; i++)
            {
                Item item = Utility.ItemIdMapper.ConstructItemByID(data.Items[i].ItemType);
                item.SaveData = data.Items[i].SaveData;

                Inventory.Add(item);
            }
        }
        
        public class ActorData
        {
            public int LocationID;
            public bool Known;
            public ItemSaveEntry[] Items;
        }

        public abstract void UseItemOnActor(Item item);
    }
}
